#ifndef UNTITLED1_FILEMANAGEMENTSERVICE_H
#define UNTITLED1_FILEMANAGEMENTSERVICE_H

#include <fstream>
#include <optional>

class FileManagementService {
    std::ifstream readStream;
    std::ofstream writeStream;

    public:
        FileManagementService(std::string, std::string);
        std::optional<char> readChar();
        void writeToFile(std::string);

};


#endif //UNTITLED1_FILEMANAGEMENTSERVICE_H
