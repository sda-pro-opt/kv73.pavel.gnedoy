#ifndef UNTITLED1_STOREMANAGEMENTSERVICE_H
#define UNTITLED1_STOREMANAGEMENTSERVICE_H

#include <map>

class StoreManagementService {
    std::map<int, std::string> codeToLexemStore;
    std::map<std::string, int> lexemToCodeStore;

    public:
        StoreManagementService(): codeToLexemStore(), lexemToCodeStore() {};
        bool isLexemAdded(std::string *);
        int getCodeByLexem(std::string *);
        std::string getLexemByCode(int);
        void addLexemToStore(int, std::string);
        void print();
        std::string printToFile();
};

#endif //UNTITLED1_STOREMANAGEMENTSERVICE_H
