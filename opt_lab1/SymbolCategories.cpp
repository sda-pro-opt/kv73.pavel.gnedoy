#include "SymbolCategories.h"
#include "CategoriesConstants.h"

using namespace std;
using namespace category;

void SymbolCategories::initCategories() {
    initErrorCategory(ERR);
    initDigitCategory(DIGIT);
    initLetterCategory(LETTER);
    initDelimiterCategory(DELIMITER);
    initMultiDelimiterCategory(MULTI_DELIMITER);
    initCommentCategory(COMMENT);
    initSpaceSignCategory(WHITESPACE);
}

void SymbolCategories::initErrorCategory(int categoryNumber) {
    for (int i = 0; i < ASCIIlenght; i++) {
        asciiByCategoriesArr[i] = categoryNumber;
    }
}

void SymbolCategories::initDigitCategory(int categoryNumber) {
    // 0..9 -> [48, ..., 57]
    const int from = 48;
    const int to = 57;

    for (int i = from; i <= to; i++) {
        asciiByCategoriesArr[i] = categoryNumber;
    }
}

void SymbolCategories::initLetterCategory(int categoryNumber) {
    const int from = 65;
    const int to = 90;

    for (int i = from; i <= to; i++) {
        asciiByCategoriesArr[i] = categoryNumber;
    }
}

void SymbolCategories::initCommentCategory(int categoryNumber) {
    const int i = 40; // '('

    asciiByCategoriesArr[i] = categoryNumber;
}

void SymbolCategories::initDelimiterCategory(int categoryNumber) {
    const int dotSignNumber = 46; // '.'
    const int semicolonNumber = 59; // ';'
    const int equalitySignNumber = 61; // '='
    const int minusSignNumber = 45; // '-'

    asciiByCategoriesArr[dotSignNumber] = categoryNumber;
    asciiByCategoriesArr[semicolonNumber] = categoryNumber;
    asciiByCategoriesArr[equalitySignNumber] = categoryNumber;
    asciiByCategoriesArr[minusSignNumber] = categoryNumber;
}

void SymbolCategories::initMultiDelimiterCategory(int categoryNumber) {
    const int colonSignNumber = 58; // ':' for ':=' in my case

    asciiByCategoriesArr[colonSignNumber] = categoryNumber;
}

void SymbolCategories::initSpaceSignCategory(int categoryNumber) {
    const int spaceSignNumber = 32; // ' '
    const int carriageSignNumber = 13;
    const int newLineSignNumber = 10;
    const int horizontalTabSignNumber = 9;
    const int verticalTabSignNumber = 11;
    const int newPageSignNumber = 12;
//    const int noname = -1;

    asciiByCategoriesArr[spaceSignNumber] = categoryNumber;
    asciiByCategoriesArr[carriageSignNumber] = categoryNumber;
    asciiByCategoriesArr[newLineSignNumber] = categoryNumber;
    asciiByCategoriesArr[horizontalTabSignNumber] = categoryNumber;
    asciiByCategoriesArr[verticalTabSignNumber] = categoryNumber;
    asciiByCategoriesArr[newPageSignNumber] = categoryNumber;
}







