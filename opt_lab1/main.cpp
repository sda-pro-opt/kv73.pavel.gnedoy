#include <iostream>
#include <vector>
#include <string>
#include "Scanner.h"

int main(int argc, char** argv) {
    std::string test = argc == 2 ? argv[1] : "test01";
    std::string filePathToRead = "../tests/" + test + "/input.sig";
    std::string filePathToWrite = "../tests/" + test + "/generated.txt";
    auto *fileManagementService = new FileManagementService(filePathToRead, filePathToWrite);
    auto *storeManagementService = new StoreManagementService();
    auto *scanner = new Scanner(
            storeManagementService,
            fileManagementService,
            new CharPositionService(),
            new SymbolCategories()
            );

    std::optional<std::vector<Token>> oTokens = scanner->doScanning();
    if (!oTokens) return 1;

    std::string results = storeManagementService->printToFile();
    for(auto code: oTokens.value()) {
        results = results +
                "Code: " + to_string(code.code) + " | " +
                "Row: " + to_string(code.row) +  " | " +
                "Column: " + to_string(code.column) + " | " +
                "Value: " + storeManagementService->getLexemByCode(code.code) + "\n";
    }
    fileManagementService->writeToFile(results);

    return 0;
}