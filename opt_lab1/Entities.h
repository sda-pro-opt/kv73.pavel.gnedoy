#ifndef UNTITLED1_ENTITIES_H
#define UNTITLED1_ENTITIES_H

namespace ent {
    enum Entities {
        DELIMITER,
        MULTI_DELIMITER,
        KEY_LEXEM,
        CONSTANT,
        IDENTIFIER,
    };
}

#endif //UNTITLED1_ENTITIES_H
