## **opt_lab1**

Project is created in Clion. So `Makefile` is located in `cmake-build-debug` directory.
As input for program is name of test folder, `test01` is used by default. 

```
cd cmake-build-debug
make
./opt_lab1 test02
```

### Notes
In folder ```report```, there are two files: report and photo that indicates that I showed this work but didn't complete an additional task in time.