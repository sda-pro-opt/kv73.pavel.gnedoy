#ifndef UNTITLED1_SYMBOLCATEGORIES_H
#define UNTITLED1_SYMBOLCATEGORIES_H

class SymbolCategories {
    const int ASCIIlenght = 127;
    int *asciiByCategoriesArr;

    void initCategories();
    //TODO: change int -> CATEGORY_NUMBER_TYPE
    void initDigitCategory(int); //category 1
    void initLetterCategory(int); // category 2
    void initDelimiterCategory(int); // category 3
    void initMultiDelimiterCategory(int); // category 4
    void initCommentCategory(int); //category 5
    void initErrorCategory(int); //category 6
    void initSpaceSignCategory(int); //category 0

public:
    SymbolCategories()  {
        asciiByCategoriesArr = new int[ASCIIlenght];

        initCategories();
    };
    int getCategoryBySymbol(int symbol) {
        return asciiByCategoriesArr[symbol];
    };
};

#endif //UNTITLED1_SYMBOLCATEGORIES_H
