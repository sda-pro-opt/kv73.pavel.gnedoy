#ifndef UNTITLED1_SCANNER_H
#define UNTITLED1_SCANNER_H

#include "vector"
#include "FileManagementService.h"
#include "CharPositionService.h"
#include "StoreManagementService.h"
#include "SymbolCategories.h"
#include "CodeGenerator.h"
#include "Token.h"

using namespace std;

class Scanner {
    vector<Token> codes;

    struct Symbol {
        char value;
        int code;
        int type;
        Position position;
        string note;
    };

    struct Lexem {
        string lexem;
        Position position;
    };

    StoreManagementService *storeManagementService;
    FileManagementService *fileManagementService;
    CharPositionService *charPositionService;
    SymbolCategories *symbolCategories;

    Lexem createStringFromVector(vector<Symbol>*);
    std::optional<Scanner::Symbol> scan();
    std::optional<Scanner::Symbol> commentProcessor(vector<Symbol>*, vector<Symbol>*, Symbol*);
    map<int, CodeGenerator> initGenerators();
    void initStore(map<int, CodeGenerator> *generatorByEntity);
    void insertLexemToStore(map<int, int>*, map<int, CodeGenerator>*, vector<Symbol>*);
    std::string printSymbols(vector<Symbol>*);
    std::string printSymbol(Symbol*);

    public:
        Scanner(
                StoreManagementService*,
                FileManagementService*,
                CharPositionService*,
                SymbolCategories*
                );
        optional<vector<Token>> doScanning();
};


#endif //UNTITLED1_SCANNER_H
