#ifndef UNTITLED1_CATEGORIESCONSTANTS_H
#define UNTITLED1_CATEGORIESCONSTANTS_H

namespace category {
    enum {
        WHITESPACE,
        DIGIT,
        LETTER,
        DELIMITER,
        MULTI_DELIMITER,
        COMMENT,
        ERR,
    };
}

#endif //UNTITLED1_CATEGORIESCONSTANTS_H
