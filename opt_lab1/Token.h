#ifndef UNTITLED1_TOKEN_H
#define UNTITLED1_TOKEN_H

struct Token {
    int code;
    int row;
    int column;

    void set(int iCode, int iRow, int iColumn) {
        code = iCode;
        row = iRow ;
        column = iColumn;
    }
};

#endif //UNTITLED1_TOKEN_H
