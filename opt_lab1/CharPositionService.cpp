#include "CharPositionService.h"

void CharPositionService::move() {
    ++this->position.currentColumn;
}

void CharPositionService::moveWithStep(int step) {
    this->position.currentColumn += step;
}

void CharPositionService::moveToNextRow() {
    this->position.currentColumn = 0;
    ++this->position.currentRow;
}

Position CharPositionService::getCurrentPosition() {
    return this->position;
}
