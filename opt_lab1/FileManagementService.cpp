#include "FileManagementService.h"

FileManagementService::FileManagementService(std::string toRead, std::string toWrite) : readStream(toRead), writeStream(toWrite, std::ios::trunc) {};

std::optional<char> FileManagementService::readChar() {
    if (this->readStream.eof()) {
        this->readStream.close();
        return std::nullopt;
    }

    char value = this->readStream.get();
    if (value == -1) {
        this->readStream.close();
        return std::nullopt;
    }
    return value;
}

void FileManagementService::writeToFile(std::string data) {
    writeStream << data;
    writeStream.close();
}
