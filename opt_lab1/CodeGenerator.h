#ifndef UNTITLED1_CODEGENERATOR_H
#define UNTITLED1_CODEGENERATOR_H

class CodeGenerator {
    int code;
    public:
        CodeGenerator(int from = 0, int to = 0): code(from) {};
        int formCode();
};

#endif //UNTITLED1_CODEGENERATOR_H
