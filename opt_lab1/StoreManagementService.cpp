#include "iostream"
#include "StoreManagementService.h"

bool StoreManagementService::isLexemAdded(std::string *lexem) {
    return this->lexemToCodeStore.find(*lexem) != this->lexemToCodeStore.end();
}

int StoreManagementService::getCodeByLexem(std::string *lexem) {
    return this->lexemToCodeStore[*lexem];
}

void StoreManagementService::addLexemToStore(int code, std::string lexem) {
    if (!this->isLexemAdded(&lexem)) {
        this->codeToLexemStore.insert(std::pair<int,std::string>(code, lexem));
        this->lexemToCodeStore.insert(std::pair<std::string, int>(lexem, code));
    }
}

void StoreManagementService::print() {
    for(auto it: codeToLexemStore) {
        std::cout << "Code: " << it.first << " | Lexem: " <<it.second << "\n";
    }

    std::cout << "-------------------------------------------------------------\n";
//
//    for(auto it: lexemToCodeStore) {
//        std::cout << "Lexem: " << it.first << " | Code: " <<it.second << "\n";
//    }
}

std::string StoreManagementService::printToFile() {
    std::string table = "";
    for(auto it: codeToLexemStore) {
        table = table + "Code: " + std::to_string(it.first) + " | Lexem: " + it.second + "\n";
    }

    return table + "-------------------------------------------------------------\n";
}

std::string StoreManagementService::getLexemByCode(int code) {
    return this->codeToLexemStore[code];
}