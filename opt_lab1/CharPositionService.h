#ifndef UNTITLED1_CHARPOSITIONSERVICE_H
#define UNTITLED1_CHARPOSITIONSERVICE_H

#include "Position.h"

class CharPositionService {
    Position position;

    public:
        void moveToNextRow();
        void moveWithStep(int);
        void move();
        Position getCurrentPosition();
};


#endif //UNTITLED1_CHARPOSITIONSERVICE_H
