#include <iostream>
#include "map"
#include "Scanner.h"
#include "CategoriesConstants.h"
#include "Entities.h"

using namespace std;

map<int, int> generateEntityByCategoryMap() {
    return map<int, int> {
            { category::LETTER, ent::IDENTIFIER },
            { category::DIGIT, ent::CONSTANT },
            { category::DELIMITER, ent::DELIMITER },
            { category::MULTI_DELIMITER, ent::MULTI_DELIMITER }
    };
}

map<int, CodeGenerator> Scanner::initGenerators() {
    struct codeRange {
        int from;
        int to;
    };

    const codeRange delimiterRange = {0, 255};
    const codeRange multiDelimiterRange = {301, 400 };
    const codeRange constantRange = {401, 500 };
    const codeRange keyLexemsRange = { 501, 600 };
    const codeRange identifierRange = { 601, 700 };

    return map<int, CodeGenerator> {
            { ent::DELIMITER, CodeGenerator(delimiterRange.from, delimiterRange.to) },
            { ent::MULTI_DELIMITER, CodeGenerator(multiDelimiterRange.from, multiDelimiterRange.to) },
            { ent::KEY_LEXEM, CodeGenerator(keyLexemsRange.from, keyLexemsRange.to) },
            { ent::CONSTANT, CodeGenerator(constantRange.from, constantRange.to) },
            { ent::IDENTIFIER, CodeGenerator(identifierRange.from, identifierRange.to) },
    };
}

void Scanner::initStore(map<int, CodeGenerator> *generatorByEntity) {
    const map<int, vector<string>> reservedLexems = {
            { ent::DELIMITER, { ";", ".", "=" } },
            { ent::MULTI_DELIMITER,  { ":=" } },
            { ent::KEY_LEXEM, { "PROGRAM", "BEGIN", "END", "CONST" } }
    };

    int code;
    for (auto entry : reservedLexems) {
        for (auto lexem : entry.second) {
            code = (*generatorByEntity)[entry.first].formCode();
            this->storeManagementService->addLexemToStore(code, lexem);
        }

    }
}

/////////////////////////////////////////////////

Scanner::Scanner(
        StoreManagementService *storeManagementService,
        FileManagementService *fileManagementService,
        CharPositionService *charPositionService,
        SymbolCategories *symbolCategories
        ) {
    this->storeManagementService = storeManagementService;
    this->fileManagementService = fileManagementService;
    this->charPositionService = charPositionService;
    this->symbolCategories = symbolCategories;
};

std::optional<vector<Token>> Scanner::doScanning() {
    map<int, int> entityByCategory = generateEntityByCategoryMap();
    map<int, CodeGenerator> generatorByEntity = this->initGenerators();
    this->initStore(&generatorByEntity);

    vector<Symbol> buffer;
    vector<Symbol> errBuffer;

    Scanner::Symbol currentSymbol;

    std::optional<Scanner::Symbol> oSymbol = scan();
    while (oSymbol) {
        currentSymbol = oSymbol.value();

        switch (currentSymbol.type) {
            case category::DIGIT: {
                int size = buffer.size();
                if (size) {
                    int prevSymbolType = buffer[size - 1].type;
                    if (prevSymbolType == category::MULTI_DELIMITER || prevSymbolType == category::DELIMITER) {
                        this->insertLexemToStore(&entityByCategory, &generatorByEntity, &buffer);
                    }
                }
                buffer.push_back(currentSymbol);
                oSymbol = this->scan();
                break;
            }
            case category::LETTER: {
                int size = buffer.size();
                if (size) {
                    int firstSymbolType = buffer[0].type;
                    int prevSymbolType = buffer[size - 1].type;
                    if (prevSymbolType == category::MULTI_DELIMITER || prevSymbolType == category::DELIMITER || firstSymbolType == category::DIGIT) {
                        this->insertLexemToStore(&entityByCategory, &generatorByEntity, &buffer);
                    }
                }
                buffer.push_back(currentSymbol);
                oSymbol = this->scan();
                break;
            }
            case category::MULTI_DELIMITER: {
                int size = buffer.size();

                if (size) {
                    this->insertLexemToStore(&entityByCategory, &generatorByEntity, &buffer);
                }

                if (currentSymbol.value == ':') { // :=
                    oSymbol = this->scan();
                    if (oSymbol) {
                        Scanner::Symbol nextSymbol = oSymbol.value();
                        if (nextSymbol.value == '=') {
                            buffer.push_back(currentSymbol);
                            buffer.push_back(nextSymbol);
                            this->insertLexemToStore(&entityByCategory, &generatorByEntity, &buffer);
                        } else {
                            currentSymbol.type = category::DELIMITER;
                            buffer.push_back(currentSymbol);
                            this->insertLexemToStore(&entityByCategory, &generatorByEntity, &buffer);
                            break;
                        }
                    } else {
                        currentSymbol.type = category::DELIMITER;
                        buffer.push_back(currentSymbol);
                        this->insertLexemToStore(&entityByCategory, &generatorByEntity, &buffer);
                        break;
                    }
                    oSymbol = this->scan();
                    break;
                }

                buffer.push_back(currentSymbol);
                oSymbol = this->scan();
                break;
            }
            case category::DELIMITER: {
                int size = buffer.size();
                int prevSymbolType = buffer[size - 1].type;
                if (size != 0 || prevSymbolType == category::MULTI_DELIMITER) {
                    this->insertLexemToStore(&entityByCategory, &generatorByEntity, &buffer);
                }
                buffer.push_back(currentSymbol);
                oSymbol = this->scan();
                break;
            }
            case category::WHITESPACE: {
                if (buffer.size() != 0) {
                    this->insertLexemToStore(&entityByCategory, &generatorByEntity, &buffer);
                }
                if (currentSymbol.value == '\n') {
                    this->charPositionService->moveToNextRow();
                } else {
                    this->charPositionService->moveWithStep(int(buffer.size()));
                }
                oSymbol = this->scan();
                break;
            }
            case category::COMMENT: {
                if (currentSymbol.value == '(') {
                    oSymbol = this->scan();
                    if (oSymbol) {
                        Scanner::Symbol nextSymbol = oSymbol.value();
                        if (nextSymbol.value == '*') { // (*
                            buffer.push_back(currentSymbol);
                            buffer.push_back(nextSymbol);
                        } else { // (
                            errBuffer.push_back(currentSymbol);
                            break;
                        }
                    } else {
                        errBuffer.push_back(currentSymbol);
                        break;
                    }
                }
                oSymbol = this->commentProcessor(&buffer, &errBuffer, &currentSymbol);
                break;
            }
            case category::ERR: {
                errBuffer.push_back(currentSymbol);
                oSymbol = this->scan();
                break;
            }
        }
    }

    if (buffer.size()) {
        this->insertLexemToStore(&entityByCategory, &generatorByEntity, &buffer);
    }

    if (errBuffer.size()) {
        this->fileManagementService->writeToFile(this->printSymbols(&errBuffer));
        return nullopt;
    }



    return this->codes;
}

std::optional<Scanner::Symbol>Scanner::scan() {
    this->charPositionService->move();

    std::optional<char> symbol = this->fileManagementService->readChar();
    if (!symbol) return std::nullopt;

    Position symbolPosition = this->charPositionService->getCurrentPosition();

    char value = symbol.value();
    return Symbol{
            .value = value,
            .code = value,
            .type = this->symbolCategories->getCategoryBySymbol(value),
            .position = symbolPosition
    };

}

void Scanner::insertLexemToStore(
        map<int, int> *entityByCategory,
        map<int, CodeGenerator> *generatorByEntity,
        vector<Symbol> *symbols
) {
    Lexem tempLexem = this->createStringFromVector(symbols);
    int firstSymbolCategory = (*symbols)[0].type;
    int code;
    if (this->storeManagementService->isLexemAdded(&tempLexem.lexem)) {
        code = this->storeManagementService->getCodeByLexem(&tempLexem.lexem);
    } else {
        int entity = (*entityByCategory)[firstSymbolCategory];
        code = (*generatorByEntity)[entity].formCode();
    }

    this->storeManagementService->addLexemToStore(code, tempLexem.lexem);
    this->codes.push_back(Token{
            .code = code,
            .row = tempLexem.position.currentRow,
            .column = tempLexem.position.currentColumn
    });
    symbols->clear();
}

Scanner::Lexem Scanner::createStringFromVector(vector<Symbol> *symbols) {
    string lexem;
    for(int i = 0; i < (*symbols).size(); i++){
        lexem += (*symbols)[i].value;
    }
    return Lexem{
            .lexem = lexem,
            .position = (*symbols)[0].position
    };
}

std::optional<Scanner::Symbol> Scanner::commentProcessor(vector<Symbol> *buffer, vector<Symbol> *errSymbols, Symbol *commentSymbol) {
    buffer->clear();

    std::optional<Scanner::Symbol> oSymbol = scan();
    if (!oSymbol) { // if eof
        return oSymbol;
    }
    Scanner::Symbol currentSymbol;

    while (oSymbol) {
        currentSymbol = oSymbol.value();
        if (currentSymbol.value == '*') {
            oSymbol = scan();
            if (oSymbol) {
                currentSymbol = oSymbol.value();
                if (currentSymbol.value == ')') {
                    return this->scan();
                }
            }
        } else {
            if (currentSymbol.value == '\n') {
                this->charPositionService->moveToNextRow();
            }
            oSymbol = scan();
        }
    }
    errSymbols->push_back(Symbol{
        .code = -1,
        .position = this->charPositionService->getCurrentPosition(),
        .note = "EOF detected",
    });
    return oSymbol;
}

std::string Scanner::printSymbol(Scanner::Symbol *symbol) {
    std::string result = "";
    if (symbol->note.length()) result +=  "Note: " + symbol->note + " | ";
    if (symbol->value) {
        std::string value(1, symbol->value);
        result += "Symbol: " + value + " | ";
    }
    return  result + "Row: " + std::to_string(symbol->position.currentRow) + " | " +
        "Column: " + std::to_string(symbol->position.currentColumn) +"\n";

}

std::string Scanner::printSymbols(vector<Symbol> *symbols) {
    std::string result = "";
    for(auto symbol: *symbols) {
        result += this->printSymbol(&symbol);
    }
    return result;
}





